#include "Params.h"
#include "SGLog.h"

using namespace std;

bool Params::parseArgs(vector<string> args) {
    if (args.size() < 2) {
        return false;
    }
    for (auto & arg : args) {
        SGLog::log() << arg << " ";
    }
    SGLog::log() << endl;

    try {
        dataset_path = args[0];
    } catch (...) {
        return false;
    }

    for (size_t i = 1; i < args.size(); ++i) {
        string next = args[i];
        if (next.length() < 4) return false;
        if (next.substr(0, 3) == "-m=") {
            string valstr = next.substr(3);
            mode = stoi(valstr);
            SGLog::log() << "set mode " << mode << endl;
        } else
        if (next.substr(0, 3) == "-d=") {
            string valstr = next.substr(3);
            directed = stoi(valstr) > 0;
            SGLog::log() << "set directed " << directed << endl;
        } else
        if (next.substr(0, 3) == "-n=") {
            string valstr = next.substr(3);
            n_order = stoi(valstr);
            SGLog::log() << "set n_order " << n_order << endl;
        }
        else {
            return false;
        }
    }
    return true;
}
