#ifndef TGPR_PARAMS_H
#define TGPR_PARAMS_H

#include <iostream>
#include "../TemporalGraph/TemporalGraphs.h"

struct Params {
    std::string dataset_path;
    bool directed = true;
    uint mode = 0;
    uint n_order = 2;
    bool parseArgs(std::vector<std::string> args);
};

#endif //TGPR_PARAMS_H
