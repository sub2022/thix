#include "thix_stream.h"

using namespace std;

uint H(vector<pair<Time,uint>> const &c, Time t) {
    uint n = c.size();
    vector <uint> bucket(n + 1);
    for(uint i = 0; i < n; i++){
        if (c[i].first <= t) break;
        uint x = c[i].second;
        if(x >= n){
            bucket[n]++;
        } else {
            bucket[x]++;
        }
    }
    uint cnt = 0;
    for (uint i = n; ; i--){
        cnt += bucket[i];
        if (cnt >= i) return i;
    }
}

uint Hfinal(vector<pair<Time,uint>> const &c) {
    uint n = c.size();
    vector <uint> bucket(n + 1);
    for(uint i = 0; i < n; i++){
        uint x = c[i].second;
        if(x >= n){
            bucket[n]++;
        } else {
            bucket[x]++;
        }
    }
    uint cnt = 0;
    for (uint i = n; ; i--){
        cnt += bucket[i];
        if (cnt >= i) return i;
    }
}

Thix_Stream::Thix_Stream(TemporalGraphStream &_tg, uint _n_order) : n_order(_n_order), tgs(_tg) {
    nOrderValsAtNodes.resize(tgs.num_nodes, vector<vector<pair<Time,uint>>>());
    out_degrees.resize(tgs.num_nodes, 0);
    last_time.resize(tgs.num_nodes, MAX_UINT_VALUE);
    for (auto &nov : nOrderValsAtNodes) {
        nov.resize(n_order+1, vector<pair<Time, uint>>());
    }
}


void Thix_Stream::update_edges() {

    for (uint i = tgs.edges.size()-1; ; --i) {

        // process current edge at time e.t
        auto &e = tgs.edges[i];

        if (last_time[e.u_id] > e.t) {
            last_time[e.u_id] = e.t;
            out_degrees[e.u_id] = nOrderValsAtNodes[e.u_id][0].size();
        }
        if (last_time[e.v_id] > e.t) {
            last_time[e.v_id] = e.t;
            out_degrees[e.v_id] = nOrderValsAtNodes[e.v_id][0].size();
        }

        // increase number of outgoing edges at t >= e.t
        nOrderValsAtNodes[e.u_id][0].push_back({e.t, 1});

        // special case n=0
        if (n_order == 0) {
            if (i == 0) break;
            continue;
        }

        nOrderValsAtNodes[e.u_id][1].push_back({e.t, out_degrees[e.v_id]});

        for (uint j = 1; j < n_order; ++j) {
            auto &hs = nOrderValsAtNodes[e.v_id][j];
            nOrderValsAtNodes[e.u_id][j + 1].push_back({e.t, H(hs, e.t)});
        }

        if (i == 0) break;
    }

}


std::vector<pair<NodeId, double>> Thix_Stream::compute_hindex() {
    vector<pair<NodeId, double>> result;

    unordered_set<NodeId> visited;

    update_edges();

    for (uint nid = 0; nid < tgs.num_nodes; ++nid) {
        vector<pair<Time,uint>> hs = nOrderValsAtNodes[nid][n_order];
        if (n_order > 0)
            result.emplace_back(nid, Hfinal(hs));
        else
            result.emplace_back(nid, hs.size());
    }

    return result;
}
