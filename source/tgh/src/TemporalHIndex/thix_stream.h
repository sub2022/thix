#ifndef TGH_THIX_STREAM_H
#define TGH_THIX_STREAM_H

#include "../TemporalGraph/TemporalGraphs.h"

struct Thix_Stream {

    explicit Thix_Stream(TemporalGraphStream &_tg, uint _n_order);

    [[nodiscard]] std::vector<std::pair<NodeId, double>> compute_hindex() ;

    uint n_order{};

    TemporalGraphStream &tgs;

    void update_edges();

    std::vector<std::vector<std::vector<std::pair<Time,uint>>>> nOrderValsAtNodes;

    std::vector<uint> out_degrees;
    std::vector<uint> last_time;

};


#endif //TGH_THIX_STREAM_H
