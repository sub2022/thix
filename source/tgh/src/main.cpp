#include "TemporalGraph/TemporalGraphs.h"
#include "Util/Params.h"
#include "Util/SGLog.h"
#include "TemporalGraph/TemporalGraphStreamProvider.h"
#include "TemporalHIndex/thix_recursive.h"
#include "TemporalHIndex/thix_stream.h"
#include <iomanip>
#include <cmath>
#include <map>

using namespace std;

void writeResultsToFile(const std::string& filename, std::vector<pair<NodeId, double>> &data, const std::string& sep) {
    std::ofstream fs;
    fs.open(filename);
    if (!fs.is_open()) {
        std::cout << "Could not write data to " << filename << std::endl;
        return;
    }
    fs << std::setprecision(50);
    for (auto &d : data)
        fs << d.first << sep << d.second << std::endl;

    fs.close();
}

Params load_params(int argc, char *argv[]) {
    vector<string> args;
    for (int i = 1; i < argc; ++i)
        args.emplace_back(argv[i]);

    Params params;
    if (!params.parseArgs(args)) {
        cout << "./tgh <dataset> -m=[0 for RECURS, 1 for STREAM] -n=[n-order] -d=[set 0 for undirected]" << endl;
        exit(0);
    }
    return params;
}


int main(int argc, char *argv[]) {
    auto params = load_params(argc, argv);
    std::cout << std::setprecision(10);

    auto tgs = loadTemporalGraph(params.dataset_path, params.directed);

    SGLog::log() << "Loading " << params.dataset_path << endl;
    SGLog::log() << "#nodes: " << tgs.num_nodes << endl;
    SGLog::log() << "#edges: " << tgs.edges.size() << endl;
    SGLog::log() << "tg loaded successfully - starting computation" << endl;

    switch (params.mode) {
        case 0 : {
            SGLog::log() << "running hindex - recursive" << endl;

            auto tg = tgs.toTemporalGraph();
            Thix_Recursive thixRecursive(tg.num_nodes, params.n_order);

            Timer timer;
            timer.start();

            auto r = thixRecursive.compute_hindex(tg);

            timer.stopAndPrintTime("Running time: ");

            writeResultsToFile(params.dataset_path + ".hix" + to_string(params.n_order), r, ",");

            unordered_set<uint> cores;
            for (auto &x: r) {
                cores.insert(x.second);
            }
            SGLog::log() << "Number of cores: " << cores.size() << endl;

            break;
        }
        case 1 : {
            SGLog::log() << "running hindex - stream" << endl;

            Thix_Stream thixStream(tgs, params.n_order);

            Timer timer;
            timer.start();

            auto r = thixStream.compute_hindex();

            timer.stopAndPrintTime("Running time: ");

            writeResultsToFile(params.dataset_path + ".hix" + to_string(params.n_order), r, ",");

            unordered_set<uint> cores;
            for (auto &x: r) {
                cores.insert(x.second);
            }
            SGLog::log() << "Number of cores: " << cores.size() << endl;

            break;
        }
        default:
            break;
    }

    return 0;
}