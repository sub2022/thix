#ifndef CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H
#define CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H

#include <cassert>
#include "TemporalGraphs.h"

TemporalGraphStream loadTemporalGraph(const std::string& path, bool directed);

#endif //CENTRALITY_TEMPORALGRAPHSTREAMPROVIDER_H
