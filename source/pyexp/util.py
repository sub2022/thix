import networkx as nx


def readClusterFile(filename, nodes):
    with open(filename) as f:
        content = f.readlines()

    m = {}
    cur_cluster = 1
    for d in content:
        cluster = d[1:-2]
        cs = cluster.split(',')
        for c in cs:
            if len(c) < 1:
                continue
            node = int(c.strip())
            m[node] = cur_cluster

        cur_cluster += 1

    for n in range(0, nodes):
        if n not in m:
            m[n] = 0

    return m


def readHixFile(filename):
    with open(filename) as f:
        content = f.readlines()
    try:
        data = [int(x.strip().split(',')[1].strip()) for x in content]
    except IndexError:
        data = [int(x.strip()) for x in content]
    m = {}
    nid = 0
    for d in data:
        m[nid] = d
        nid += 1
    return m


def readFile(filename):
    with open(filename) as f:
        content = f.readlines()
    data = [x.strip() for x in content]
    return data


def load_tg(filename, hi):
    data = readFile(filename)
    edges = {}
    for d in data:
        s = d.split(' ')
        if len(s) < 4: continue
        if (int(s[0]), int(s[1])) not in edges:
            edges[(int(s[0]), int(s[1]))] = 0
        edges[(int(s[0]), int(s[1]))] += 1

    G = nx.Graph()
    for d in edges.keys():
        u = d[0]
        v = d[1]
        w = edges[d]
        G.add_edge(u, v, weight=w)

    if hi is not None:
        for n, d in G.nodes(data=True):
            d['weight'] = hi[n]

    return G
